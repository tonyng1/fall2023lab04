package debugging.assignment;

/**
 * This class is meant for storing the information about an Employee
 */
public class Employee {
    private String name;
    private int employeeId;

    /**
     * Constructor that takes as input an initializes an Employee object
     * 
     * @param name       The name of the Employee
     * @param employeeId The Employee id
     */
    public Employee(String name, int employeeId) {
        this.name = name;
        this.employeeId = employeeId;
    }

public Employee(Employee other){
    this(other.name, other.employeeId);
}

    /**
     * Accessor method for getting the name.
     * 
     * @return The name of the Employee
     */
    public String getName() {
        return name;
    }

    /**
     * Accessor method for getting the employee id.
     * 
     * @return The employee id of the employee
     */
    public int getEmployeeId() {
        return employeeId;
    }

    /**
     * Mutator method for setting the name field
     * 
     * @param name The new name to set the object to be
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Mutator method for setting the employee id field
     * 
     * @param employeeId The new employee id
     */
    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    /* */
    @Override
    public boolean equals(Object o) {
        Employee e = (Employee) o;
        return this.name.equals(e.name) && this.employeeId == e.employeeId;
    }

}
