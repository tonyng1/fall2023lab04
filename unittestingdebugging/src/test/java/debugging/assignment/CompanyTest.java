package debugging.assignment;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test Company class
 */
public class CompanyTest
{
    /**
     * Checks for different employees
     */
    @Test
    public void testDifferentEmployees()
    {
        Employee[] employees = new Employee[] { new Employee("Swetha", 12345), new Employee("Dan", 123), new Employee("Victoria", 12)};

        Company c1 = new Company(employees);
        System.out.println(c1);
        // change it so that it's a different company

        employees[0].setEmployeeId(12);

        Company c2 = new Company(employees);
        System.out.println(c2);
        assertNotEquals(c1, c2);
        
    }
}
